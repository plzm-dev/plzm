defmodule Plzm.Repo do
  use Ecto.Repo,
    otp_app: :plzm,
    adapter: Ecto.Adapters.Postgres
end
