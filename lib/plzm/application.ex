defmodule Plzm.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      PlzmWeb.Telemetry,
      Plzm.Repo,
      {DNSCluster, query: Application.get_env(:plzm, :dns_cluster_query) || :ignore},
      {Phoenix.PubSub, name: Plzm.PubSub},
      # Start the Finch HTTP client for sending emails
      {Finch, name: Plzm.Finch},
      # Start a worker by calling: Plzm.Worker.start_link(arg)
      # {Plzm.Worker, arg},
      # Start to serve requests, typically the last entry
      PlzmWeb.Endpoint
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Plzm.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    PlzmWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
