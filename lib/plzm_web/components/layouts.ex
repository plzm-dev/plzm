defmodule PlzmWeb.Layouts do
  use PlzmWeb, :html

  embed_templates "layouts/*"
end
