defmodule PlzmWeb.PageHTML do
  use PlzmWeb, :html

  embed_templates "page_html/*"
end
